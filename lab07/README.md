# Лабораторна робота №7. Функції
## Мета

Переробити програми, що були розроблені під час виконання минулих лабораторних робіт з використанням функцій.

### 1.1 Розробник

Інформація про розробника:
- Москаль Кирило Артемович; 
- КІТ 121б;

### 1.2 Загальне завдання

- Самостійно перетворити код с 6 та 5 лабораторной роботи за допомогою функцій
- Завантажити код на віддалений репозиторій

### 1.3 Задача
- реалізовати функції для завдання на їх визов у main();
- Завантажити код на свій gitlab;

## Пошагові дії, які призвели до необхідного результата
> - Створив директорію lab07
> - Створив директорію src;
> - Створив файл main.c;
> - Реалізую функції до існуючих алгоритмів;

> - Створюэмо main() з визовом кожної функції та змінних с параметрами, змінних с відповідями
```
int main()
{
	int number[10] = { 4, 8, 1, 3, 12, 10, 6, 2, 7, 11 };
	int answer = compare(number);
	int a = 72, b = 20; // вводим изначальные переменные
	// направляем переменные в циклы
	_1for(a, b);
	_2while(a, b);
	_3dowhile(a, b);
}
```

> - Створюэмо функцію для підрахунку чисел, які більше за числа праворуч и ліворуч.
```
int compare(int *number)
{
	int count = 0;
	for (int i = 1; i < 10; i++) {
		if (number[i] > number[i - 1] && number[i] > number[i + 1])
			count++;
	}
	return count;
}
```

> - Створуємо функцію для for(), while(), do_while()
```
void _1for(int c, int d) // алгоритм Евклида сделанный при помощи оператора for
{
	for (int i = 1; i > 0;) {
		if (c != 0 && d != 0) {
			if (c > d) {
				c = c % d;
			} else
				d = d % c;
		} else
			break;
	}
	nod1 = d + c;
}

void _2while(int c, int d) // алгоритм Евклида сделанный при помощи оператора while
{
	while (1) {
		if (c != 0 && d != 0) {
			if (c > d) {
				c = c % d;
			} else
				d = d % c;
		} else
			break;
	}
	nod2 = d + c;
}

void _3dowhile(int c, int d) // алгоритм Евклида сделанный при помощи оператора do whole
{
	do {
		if (c != 0 && d != 0) {
			if (c > d) {
				c = c % d;
			} else
				d = d % c;
		} else
			break;
	} while (1);
	nod3 = d + c;
}

```
## Посилання на створений gitlab аккаунт:
-Gitlab "https://gitlab.com/Marapbl4"

## Висновок
На цій лабораторній работі я реліозовав свої алгоритми до функції та поєднал у одній програмі

