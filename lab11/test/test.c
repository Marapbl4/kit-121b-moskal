
#include "lib.h"
#include <check.h>

START_TEST(diagonal_test)
{
    double **matrix = (double **)malloc(NUMBER_OF_COLS_AND_ROWS * sizeof(double *));
    for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
        *(matrix + i) = (double *)malloc(NUMBER_OF_COLS_AND_ROWS * sizeof(double));
    }

    double **reversed_matrix = (double **)malloc(NUMBER_OF_COLS_AND_ROWS * sizeof(double *));
    for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
        *(reversed_matrix + i) = (double *)malloc(NUMBER_OF_COLS_AND_ROWS * sizeof(double));
    }

    double expected_matrix[NUMBER_OF_COLS_AND_ROWS][NUMBER_OF_COLS_AND_ROWS] = {
            {0.5, -0.5, -0.3},
            {0, 1, -0.2},
            {0, 0, 0.2}
    };

    seed_matrix(matrix);
    reverse_matrix(matrix, reversed_matrix);

    for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
        for (int j = 0; j < NUMBER_OF_COLS_AND_ROWS; j++) {
            ck_assert_int_eq(expected_matrix[i][j], *(*(reversed_matrix + i) + j));
        }
    }

    for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
        free(*(matrix + i));
        free(*(reversed_matrix + i));
    }
    free(matrix);
    free(reversed_matrix);
}
END_TEST

int main(void)
{
    Suite *s = suite_create("Programming");
    TCase *tc_core = tcase_create("lab11");

    tcase_add_test(tc_core, diagonal_test);
    suite_add_tcase(s, tc_core);

    SRunner *sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
