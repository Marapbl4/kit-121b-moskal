#include "lib.h"

void seed_matrix(double **matrix)
{
	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
		for (int j = 0; j < NUMBER_OF_COLS_AND_ROWS; j++) {
			double typed_element = 0;

		input:
			printf("Будьласка, введіть значення (від -1000 до 1000) для елементу матриці у %d рядку і %d стовпчику: ",
			       i + 1, j + 1);
			scanf("%3lf", &typed_element);
			if (typed_element < -1000 || typed_element > 1000) {
				printf("Ви ввели неправильне значення, повторіть спробу.\n");
				goto input;
			}
			*(*(matrix + i) + j) = typed_element;
		}
	}
}

void print_matrix(double **matrix)
{
	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
		printf("[");
		for (int j = 0; j < NUMBER_OF_COLS_AND_ROWS; j++)
			printf("%.2f\t", *(*(matrix + i) + j));
		printf("]\n");
	}
}

double matrix_determinant_count(double **matrix)
{
	return (*(*(matrix + 0) + 0)) *
		       (((*(*(matrix + 1) + 1)) * (*(*(matrix + 2) + 2))) -
			((*(*(matrix + 2) + 1)) * (*(*(matrix + 1) + 2)))) -
	       (*(*(matrix + 0) + 1)) *
		       ((*(*(matrix + 1) + 0)) * (*(*(matrix + 2) + 2)) -
			(*(*(matrix + 2) + 0)) * (*(*(matrix + 1) + 2))) +
	       (*(*(matrix + 0) + 2)) *
		       ((*(*(matrix + 1) + 0)) * (*(*(matrix + 2) + 1)) -
			(*(*(matrix + 2) + 0)) * (*(*(matrix + 1) + 1)));
}

void transpose_matrix(double **matrix, double **result_matrix)
{
	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
		for (int j = 0; j < NUMBER_OF_COLS_AND_ROWS; j++) {
			*(*(result_matrix + i) + j) = *(*(matrix + j) + i);
		}
	}
}

double math_addition(double **matrix, int row, int col)
{
	double result = 0;
	double **b = (double **)malloc(
		NUMBER_OF_COLS_AND_ROWS_IN_MATH_ADDITION * sizeof(double *));
	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS_IN_MATH_ADDITION; i++) {
		*(b + i) = (double *)malloc(
			NUMBER_OF_COLS_AND_ROWS_IN_MATH_ADDITION *
			sizeof(double));
	}
	int i, j, bi, bj;

	for (i = 0, bi = 0; i < 3; i++) {
		if (i != row) {
			for (j = 0, bj = 0; j < 3; j++)
				if (j != col) {
					*(*(b + bi) + bj) =
						*(*(matrix + i) + j);
					bj++;
				}
			bi++;
		}
	}

	if ((row + col) % 2) {
		result = *(*(b + 0) + 1) * *(*(b + 1)) -
			 *(*(b + 0) + 0) * *(*(b + 1) + 1);
	} else {
		result = *(*(b + 0) + 0) * *(*(b + 1) + 1) -
			 *(*b + 1) * *(*(b + 1));
	}

	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS_IN_MATH_ADDITION; i++) {
		free(*(b + i));
	}
	free(b);

	return result;
}

bool reverse_matrix(double **matrix, double **result_matrix)
{
	bool reversing_result;

	/* Створення транспонованої матриці */
	double **transposed_matrix =
		(double **)malloc(NUMBER_OF_COLS_AND_ROWS * sizeof(double *));
	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
		*(transposed_matrix + i) = (double *)malloc(
			NUMBER_OF_COLS_AND_ROWS * sizeof(double));
	}

	/* Створення й розрахунок детермінанта */
	double matrix_determinant = matrix_determinant_count(matrix);
	/* Якщо детермінант менше 0, то оберненої матриці не існує */
	if (matrix_determinant == 0) {
		printf("Детермінант матриці дорівнює 0, оберненої матриці не існує.\n");
		reversing_result = false;
	} else {
		/* Транспонування матриці */
		transpose_matrix(matrix, transposed_matrix);
		/* Головний алгоритм розрахунку оберненої матриці */
		for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
			for (int j = 0; j < NUMBER_OF_COLS_AND_ROWS; j++) {
				*(*(result_matrix + i) + j) =
					(1 / matrix_determinant) *
					math_addition(transposed_matrix, i, j);
			}
		}
		reversing_result = true;
	}

	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
		free(*(transposed_matrix + i));
	}
	free(transposed_matrix);

	return reversing_result;
}