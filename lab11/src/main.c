
#include "lib.h"

/**
 * Точка входу
 * @return код завершення (0)
 */
int main()
{
	/* Створення матриці */
	double **matrix =
		(double **)malloc(NUMBER_OF_COLS_AND_ROWS * sizeof(double *));
	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
		*(matrix + i) = (double *)malloc(NUMBER_OF_COLS_AND_ROWS *
						 sizeof(double));
	}

	/* Створення перевернутої матриці */
	double **reversed_matrix =
		(double **)malloc(NUMBER_OF_COLS_AND_ROWS * sizeof(double *));
	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
		*(reversed_matrix + i) = (double *)malloc(
			NUMBER_OF_COLS_AND_ROWS * sizeof(double));
	}

	/* Заповнюемо */
	printf("Заповнюємо матрицю\n");
	seed_matrix(matrix);

	/* Вивід матриці */
	printf("Матриця:\n");
	print_matrix(matrix);

	/* Розрахунок матриці */
	bool isset_reversing_matrix = reverse_matrix(matrix, reversed_matrix);

	/* Вивід перевернутої матриці */
	if (isset_reversing_matrix) {
		printf("Перевернута матриця:\n");
		print_matrix(reversed_matrix);
	}

	/* Очищення пам'яті */
	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
		free(*(matrix + i));
		free(*(reversed_matrix + i));
	}
	free(matrix);
	free(reversed_matrix);

	return 0;
}
