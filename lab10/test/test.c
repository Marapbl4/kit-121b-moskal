

#include "lib.h"
#include <check.h>

START_TEST(diagonal_test)
{
    int expected[NUMBER_OF_COLS_AND_ROWS] = {0, 1, 2};
    int **matrix = (int **)malloc(NUMBER_OF_COLS_AND_ROWS * sizeof(int *));
    for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
        *(matrix + i) =
                (int *)malloc(NUMBER_OF_COLS_AND_ROWS * sizeof(int));
    }
    /* Заповнюемо матрицю */
    for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
        for (int j = 0; j < NUMBER_OF_COLS_AND_ROWS; j++) {
            *(*(matrix + i) + j) = j; // matrix[i][i] = j;
        }
    }

    int *diagonal_elements =
            (int *)malloc(NUMBER_OF_COLS_AND_ROWS * sizeof(int));
    /* Заповнюю майбутній масив з елементами діагоналей */
    for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
        *(diagonal_elements + i) = 0; // diagonal_elements = 0;
    }

    /* Викликаю функцію, що бере елементи з діагоналі матриці й розташовує їх у порядку зростання */
    diagonal_function(matrix, diagonal_elements);

    for(int i=0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
        int actual = *(diagonal_elements + i);
        ck_assert_int_eq(actual, expected[i]);
    }

    for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
        free(*(matrix + i));
    }
    free(matrix);
    free(diagonal_elements);
}
END_TEST

int main(void)
{
    Suite *s = suite_create("Programming");
    TCase *tc_core = tcase_create("Lab10");

    tcase_add_test(tc_core, diagonal_test);
    suite_add_tcase(s, tc_core);

    SRunner *sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
