#include "lib.h"

void diagonal_function(int **matrix, int *diagonal_elements)
{
	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
		for (int j = 0; j < NUMBER_OF_COLS_AND_ROWS; j++) {
			if (i == j) {
				*(diagonal_elements + i) = *(
					*(matrix + i) +
					j); // diagonal_elements[i] = matrix[i][j];
			}
		}
	}
	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS - 1; i++) {
		for (int j = 0; j < NUMBER_OF_COLS_AND_ROWS - i - 1; j++) {
			if (*(diagonal_elements + j) >
			    *(diagonal_elements + (j + 1))) {
				int current_element =
					*(diagonal_elements + (j));
				*(diagonal_elements + (j)) =
					*(diagonal_elements + (j + 1));
				*(diagonal_elements + (j + 1)) =
					current_element;
			}
		}
	}
}
