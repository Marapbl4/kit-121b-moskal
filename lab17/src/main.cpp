

#include "lib.h"
#include "entity.h"

/**
 * Точка входу
 * @return код заверщення (0)
 */
int main()
{
	/* конструкція */
	DynamicArrayContainer worksArrayContainer(3);

	worksArrayContainer.getElement(0)
		.getWorkStructure()
		.setAmountOfOpenExercises(10);
	worksArrayContainer.getElement(0).setStudentLastName("Semen");
	worksArrayContainer.getElement(1).setStudentLastName("Stepan");
	worksArrayContainer.getElement(2).setStudentLastName("Kirill");
	worksArrayContainer.print();

	cout << "After filtration: " << endl;

	ModalWork *Bodya = new ModalWork();
	Bodya->setStudentLastName("Bodya");
	worksArrayContainer.addElement(*Bodya);
	worksArrayContainer.removeElement(0);

	/* копіювання кострукції в роботу */
	DynamicArrayContainer new_array(worksArrayContainer);

	new_array.sortWorksByQuestions();

	new_array.print();

	return 0;
}
