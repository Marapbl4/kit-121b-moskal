#include <iostream>
#include <string>
#include <cstdio>
using namespace std;

/** Константа для максимальної кількості студентів */
#define MARK 100

/** Константа розміру */
#define DYNAMIC_ARRAY_SIZE 1

/** Предмети студентів */
enum WorkSubject { PROGA, FIZRA, ELECTRONIC };

/** Клас для модуля work structure */
class ModalWorkStructure {
    private:
	int amount_of_practical_exercises;
	int amount_of_theoretical_questions;
	int amount_of_open_exercises;

    public:
	ModalWorkStructure()
		: amount_of_practical_exercises(5),
		  amount_of_theoretical_questions(5),
		  amount_of_open_exercises(5)
	{
	}
	ModalWorkStructure(int amount_of_practical_exercises,
			   int amount_of_theoretical_questions,
			   int amount_of_open_exercises);
	ModalWorkStructure(const ModalWorkStructure &copy);

	~ModalWorkStructure();

	int getAmountOfPracticalExercises() const;
	void setAmountOfPracticalExercises(int practise);

	int getAmountOfTheoreticalQuestions() const;
	void setAmountOfTheoreticalQuestions(int theory);

	int getAmountOfOpenExercises() const;
	void setAmountOfOpenExercises(int open_task);

	void ModalWorkStructurePrint() const;
};

/** Клас для моудля control work */
class ModalWork {
    private:
	bool checked;
	string student_last_name;
	int mark;
	ModalWorkStructure work_structure;
	WorkSubject work_subject;

    public:
	ModalWork()
		: checked(false), student_last_name("Noname"), mark(0),
		  work_structure(), work_subject(PROGA)
	{
	}
	ModalWork(bool checked, string student_last_name, int mark,
		  WorkSubject work_subject, int theory_amount,
		  int practice_amount, int open_task_amount);
	ModalWork(const ModalWork &copy);

	~ModalWork();

	bool getChecked() const;
	void setChecked(bool checked);

	string getStudentLastName() const;
	void setStudentLastName(string student_last_name);

	int getMark() const;
	void setMark(int mark);

	ModalWorkStructure &getWorkStructure();
	void setWorkStructure(const ModalWorkStructure &work_structure);

	WorkSubject getWorkSubject() const;
	void setWorkSubject(WorkSubject work_subject);

	void ModalWorkPrint() const;
};
