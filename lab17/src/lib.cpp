#include "lib.h"

/*ModalWorkStructure клас метод */
ModalWorkStructure::ModalWorkStructure(int amount_of_practical_exercises,
				       int amount_of_theoretical_questions,
				       int amount_of_open_exercises)
{
	ModalWorkStructure::amount_of_open_exercises = amount_of_open_exercises;
	ModalWorkStructure::amount_of_theoretical_questions =
		amount_of_theoretical_questions;
	ModalWorkStructure::amount_of_practical_exercises =
		amount_of_practical_exercises;
}

ModalWorkStructure::ModalWorkStructure(const ModalWorkStructure &copy)
{
	ModalWorkStructure::amount_of_practical_exercises =
		copy.amount_of_practical_exercises;
	ModalWorkStructure::amount_of_theoretical_questions =
		copy.amount_of_theoretical_questions;
	ModalWorkStructure::amount_of_open_exercises =
		copy.amount_of_open_exercises;
}

ModalWorkStructure::~ModalWorkStructure() = default;

int ModalWorkStructure::getAmountOfPracticalExercises() const
{
	return ModalWorkStructure::amount_of_practical_exercises;
}
void ModalWorkStructure::setAmountOfPracticalExercises(int practise)
{
	ModalWorkStructure::amount_of_practical_exercises = practise;
}

int ModalWorkStructure::getAmountOfTheoreticalQuestions() const
{
	return ModalWorkStructure::amount_of_theoretical_questions;
}
void ModalWorkStructure::setAmountOfTheoreticalQuestions(int theory)
{
	ModalWorkStructure::amount_of_theoretical_questions = theory;
}

int ModalWorkStructure::getAmountOfOpenExercises() const
{
	return ModalWorkStructure::amount_of_open_exercises;
}
void ModalWorkStructure::setAmountOfOpenExercises(int open_task)
{
	ModalWorkStructure::amount_of_open_exercises = open_task;
}

void ModalWorkStructure::ModalWorkStructurePrint() const
{
	printf("Amount of theory: %d, amount of practice: %d, amount of open tasks: %d\n",
	       ModalWorkStructure::amount_of_theoretical_questions,
	       ModalWorkStructure::amount_of_practical_exercises,
	       ModalWorkStructure::amount_of_open_exercises);
}

/* Init of ModalWork class methods */
ModalWork::ModalWork(bool checked, string student_last_name, int mark,
		     WorkSubject work_subject, int theory_amount,
		     int practice_amount, int open_task_amount)
{
	ModalWork::checked = checked;
	ModalWork::student_last_name = student_last_name;
	ModalWork::mark = mark;
	ModalWork::work_subject = work_subject;
	ModalWork::work_structure.setAmountOfTheoreticalQuestions(
		theory_amount);
	ModalWork::work_structure.setAmountOfPracticalExercises(
		practice_amount);
	ModalWork::work_structure.setAmountOfOpenExercises(open_task_amount);
}

ModalWork::ModalWork(const ModalWork &copy)
{
	ModalWork::checked = copy.checked;
	ModalWork::student_last_name = copy.student_last_name;
	ModalWork::mark = copy.mark;
	ModalWork::work_structure = copy.work_structure;
	ModalWork::work_subject = copy.work_subject;
}

ModalWork::~ModalWork() = default;

bool ModalWork::getChecked() const
{
	return ModalWork::checked;
}
void ModalWork::setChecked(bool checked)
{
	ModalWork::checked = checked;
}

string ModalWork::getStudentLastName() const
{
	return ModalWork::student_last_name;
}
void ModalWork::setStudentLastName(string student_last_name)
{
	ModalWork::student_last_name = student_last_name;
}

int ModalWork::getMark() const
{
	return ModalWork::mark;
}
void ModalWork::setMark(int mark)
{
	if (mark < MAX_MARK) {
		ModalWork::mark = mark;
	} else {
		cout << "Mark is over 100, corruption!" << endl;
	}
}

ModalWorkStructure &ModalWork::getWorkStructure()
{
	return ModalWork::work_structure;
}
void ModalWork::setWorkStructure(const ModalWorkStructure &work_structure)
{
	ModalWork::work_structure = work_structure;
}

WorkSubject ModalWork::getWorkSubject() const
{
	return ModalWork::work_subject;
}
void ModalWork::setWorkSubject(WorkSubject work_subject)
{
	ModalWork::work_subject = work_subject;
}

void ModalWork::ModalWorkPrint() const
{
	cout << "Work checked: " << ModalWork::checked
	     << ", student name: " << ModalWork::student_last_name
	     << ", mark: " << ModalWork::mark << "." << endl;
	ModalWork::work_structure.ModalWorkStructurePrint();
}
