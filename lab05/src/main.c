int nod1,nod2,nod3; // вводим переменные для вывода ответа

void _1for(int c,int d) // алгоритм Евклида сделанный при помощи оператора for
{
    for(int i=1; i>0;)
    {
        if(c!=0 && d!=0)
        {
            if(c>d)
            {
                c=c%d;
            }
            else
                d=d%c;
        }
        else
        break;
    }
    nod1=d+c;
}

void _2while(int c,int d) // алгоритм Евклида сделанный при помощи оператора while
{
    while(1)
    {
        if(c!=0 && d!=0)
        {
            if(c>d)
            {
                c=c%d;
            }
            else
                d=d%c;
        }
        else
        break;
        }
    nod2=d+c;
}

void _3dowhile(int c,int d) // алгоритм Евклида сделанный при помощи оператора do whole
{
    do
    {
        if(c!=0 && d!=0)
        {
            if(c>d)
            {
                c=c%d;
            }
            else
                d=d%c;
        }
        else
        break;
    }
    while(1);
    nod3=d+c;
}

int main()
{
    int a=72, b=20; // вводим изначальные переменные
    // направляем переменные в циклы 
    _1for(a,b); 
    _2while(a,b);
    _3dowhile(a,b);
}
