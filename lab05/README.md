**№5. Розробка програм, що розгалужуються**

**Мета**: Реалізувати програму за допомогою трьох типів циклів: for, while-do, do-while

Інформація про розробника:
- Москаль К.А.;
- НТУ “ХПІ”, факультет “КІТ”, група 121-б.;

**Загальне завдання:**
Визначити найбільший спільний дільник для двох заданих чисел.


**Хід роботи:**

1. Створити репозиторій за структурою:
└── lab05
├── Makefile
├── README.md
├── doc
│ └── lab05.txt
└── src
└── main.c
2. Пишемо программу у файлі main.c
2.1 Вводимо початкові змінні та направляємо їх у цикл:
`int main()
{
    int a=72, b=20; 
    // направляем переменные в циклы 
    _1for(a,b); 
    _2while(a,b);
    _3dowhile(a,b);
}
`
2.2 Вводимо змінні для виводу відповіді:
`int nod1,nod2,nod3; 
2.3 Виконуємо цикл Евкліда за допомогою оператора for:
`void _1for(int c,int d) 
{
    for(int i=1; i>0;)
    {
        if(c!=0 && d!=0)
        {
            if(c>d)
            {
                c=c%d;
            }
            else
                d=d%c;
        }
        else
        break;
    }
    nod1=d+c;
}
` 
2.4 Виконуємо цикл Евкліда за допомогою оператора while:
`void _2while(int c,int d) 
{
    while(1)
    {
        if(c!=0 && d!=0)
        {
            if(c>d)
            {
                c=c%d;
            }
            else
                d=d%c;
        }
        else
        break;
        }
    nod2=d+c;
}
`
2.5 Виконуємо цикл Евклида за допомогою оператора do while:
`void _3dowhile(int c,int d) 
{
    do
    {
        if(c!=0 && d!=0)
        {
            if(c>d)
            {
                c=c%d;
            }
            else
                d=d%c;
        }
        else
        break;
    }
    while(1);
    nod3=d+c;
}
`
3. Компілюємо программу та перевіряємо на працездатність.
4. Завантажуємо программу до репозиторію командою git push

**Висновок**
Навчився створювати программи що розгалужуются. 
