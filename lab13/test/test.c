
#include "lib.h"
#include <check.h>

START_TEST(str_to_digit_test)
{
    /* Ініціювання змінних для программи */
    int amplitude;
    int period;
    char seeder;
    int expected_amplitude = 1;
    int expected_period = 0;
    char expected_seeder = '*';

    /* Создаємо територію, в якій будемо малювати й заповнюємо її */
    int **result = (int **)malloc(SIZE_OF_GRID * sizeof(int *));
    for (int i = 0; i < SIZE_OF_GRID; i++) {
        *(result + i) = (int *)malloc(SIZE_OF_GRID * sizeof(int));
    }
    for (int i = 1; i < SIZE_OF_GRID; i++) {
        for (int j = 1; j < SIZE_OF_GRID; j++) {
            *(*(result + i) + j) = ' ';
        }
    }


    /* Малюємо синусоїду в нашій території */
    draw_sine_wave(amplitude, period, seeder, result);

    /* Отримання данних з вхідного файлу */
    FILE *file = fopen("./assets/input.txt", "r");
    if (file != NULL) {
        fscanf(file, "%d %d %c", &amplitude, &period, &seeder);
    } else {
        printf("Error with opening file.\n");
        exit(1);
    }
    fclose(file);

    /* Перевіряємо на валідність созданої синусоїди */
    ck_assert_int_eq(expected_amplitude, amplitude);
    ck_assert_int_eq(expected_period, period);

    /* Чистемо памьять після використовування */
    for (int i = 0; i < SIZE_OF_GRID; i++) {
        free(*(result + i));
    }
    free(result);
}
END_TEST

int main(int argc, char **argv)
{
    Suite *s = suite_create("Programming");
    TCase *tc_core = tcase_create("Lab13");

    tcase_add_test(tc_core, str_to_digit_test);
    suite_add_tcase(s, tc_core);

    SRunner *sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
