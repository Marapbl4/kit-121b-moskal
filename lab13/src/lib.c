#include "lib.h"

void draw_sine_wave(int amplitude, int period, char seeder, int **grid)
{
	double x, y;

	for (x = -3.14; x <= 3.14; x += 0.1) {
		y = amplitude * sin(x + period);
		plot((int)(x * 10), (int)(y * 10), grid, seeder);
	}
}

void plot(int x, int y, int **grid, char seeder)
{
	int center_y = SIZE_OF_GRID / 2;
	int center_x = SIZE_OF_GRID / 2;

	*(*(grid + (center_y - y)) + (center_x - x)) = seeder;
}

void print_grid(int **grid)
{
	for (int i = 0; i < SIZE_OF_GRID; i++) {
		for (int j = 0; j < SIZE_OF_GRID; j++) {
			putchar(*(*(grid + i) + j));
		}
		putchar('\n');
	}
}
