

#include "lib.h"

/**
 * Точка входу
 * @return код завершення (0)
 */
int main(int argc, char **argv)
{
	/* Ініціювання змінних, що зберігають у собі дирректорії з вхідним файлом та вихідним файлом */
	char *input_dir = *(argv + 1), *output_dir = *(argv + 2);

	/* Ініціювання змінних для программи */
	int amplitude, period;
	char seeder;

	/* Создаємо територію, в якій будемо малювати й заповнюємо її */
	int **result = (int **)malloc(SIZE_OF_GRID * sizeof(int *));
	for (int i = 0; i < SIZE_OF_GRID; i++) {
		*(result + i) = (int *)malloc(SIZE_OF_GRID * sizeof(int));
	}
	for (int i = 1; i < SIZE_OF_GRID; i++) {
		for (int j = 1; j < SIZE_OF_GRID; j++) {
			*(*(result + i) + j) = ' ';
		}
	}

	/* Отримання данних з вхідного файлу */
	printf("Отримую з %s й кладу в %s\n", input_dir, output_dir);
	FILE *file = fopen(input_dir, "r");
	if (file != NULL) {
		fscanf(file, "%d %d %c", &amplitude, &period, &seeder);
	} else {
		printf("Error with opening file.\n");
		exit(1);
	}
	fclose(file);

	/* Малюємо синусоїду в нашій території */
	draw_sine_wave(amplitude, period, seeder, result);

	/* Відображаємо її у консолі */
	print_grid(result);

	/* Кладемо результат у вихідний файл */
	FILE *output_file = fopen(output_dir, "a+");
	for (int i = 0; i < SIZE_OF_GRID; i++) {
		for (int j = 0; j < SIZE_OF_GRID; j++) {
			fprintf(output_file, "%c", *(*(result + i) + j));
		}
		fprintf(output_file, "%c", '\n');
	}
	fclose(output_file);

	/* Чистемо памьять після використовування */
	for (int i = 0; i < SIZE_OF_GRID; i++) {
		free(*(result + i));
	}
	free(result);

	return 0;
}
