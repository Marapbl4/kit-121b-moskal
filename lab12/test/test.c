

#include "lib.h"
#include <check.h>

START_TEST(str_to_test)
{
    /* Ініціалізація змінних */
    double expected = 321.65;
    char *stroka = malloc(MAX_C);
    double actual = 0;

    /* Отримуємо ввід данних від користувача */
    fgets(stroka, MAX_C, stdin);

    /* Конвертуемо строку в число */
    actual = str_to(stroka);

    /* Перевіряемо данні на валідність */
    ck_assert_double_eq(actual, expected);

    
}
END_TEST

int main(void)
{
    Suite *s = suite_create("Programming");
    TCase *tc_core = tcase_create("lab12");

    tcase_add_test(tc_core, str_to_digit_test);
    suite_add_tcase(s, tc_core);

    SRunner *sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
