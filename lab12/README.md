# Лабораторна робота №12. Строки Null-terminated C Strings

## Мета

Отримати навички розробки програм мовою С, які моють у собі строки Null-terminated C Strings

## 1 Вимоги

### 1.1 Розробник

Інформація

- Москаль Кирило Артемович;
- группа КН-921б;

### 1.2 Загальне завдання

- Створити програму, яка буде конвертувати число, яке подано у вигляді строки у реальне число (type: int, float);


### 1.3 Задача
- Зробити функцію, що буде перераховувати кожен елемент строки й перетворювати її у цифру.

- У тестах введення брати з файлу `assets/input.txt`;

  за допомогою команди `cat ./assets/input.txt` `./dist/main.bin` та функції `fgets`;
  
- Максимаьна кількість символів, що можна зчитати з файлу = 100 символів.

> Для того, щоб запустити программу та побачити її роботу, достатьно виповнити bash команду:
>
> `./dist/main.bin`.
>
> Зробити документацію до проекта можна за допомогую команди `make docgen`.
>
> Зробити модульні тести до ціеї програми можна за допомогою команди `make test`.

Корректний результат виконання программи:

```c
make
clang-format src/* -i
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping src/lib.c -o lib.o
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping src/main.c -o main.o
mkdir -p dist
clang -fprofile-instr-generate -fcoverage-mapping lib.o main.o -o dist/main.bin
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping -Isrc test/test.c -o test.o
clang -fprofile-instr-generate -fcoverage-mapping lib.o test.o -lcheck -lm -lrt -lpthread -lsubunit -o dist/test.bin
./dist/main.bin
Введіть число, яке ви хочете конвертувати у тип int чи float: 
321.65
Результат конвертації строки: 321.65
```

## Висновок

На цій лабораторній работі я навчився користуватися програмами, що мають у собі Null-terminated C Strings, а також функцію fgets.