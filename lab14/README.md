# Лабораторна робота №14. Структуровані типи даних

## Мета

Отримати навички розробки програм мовою С, які моють у собі структуровані типи данних

## 1 Вимоги

### 1.1 Розробник

Інформація

- Москаль Кирило Артемович;
- группа КН-921б;

### 1.2 Загальне завдання

- Розобити программу, що фільтрує массив з обьєктами заданої структури за значиними критеріями
- Зробити функції взаємодії з файлами

### 1.3 Задача
- Зробити функцію, що буде витягувати з файлу данні та перетворбвати іх у структурні данні;
- Зробити функцію фільтрування (перебирати кожен елемент, та перевіряти на ідентичність з фільтратором);


> Для того, щоб запустити программу та побачити її роботу, достатьно виповнити bash команду:
>
> `./dist/main.bin "./assets/input.txt" "./dist/output.txt"`.
>
> Зробити документацію до проекта можна за допомогую команди `make docgen`.
>
> Зробити модульні тести до ціеї програми можна за допомогою команди `make test`.

Корректний результат виконання программи:

```c
make
clang-format src/* -i
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping src/lib.c -o lib.o
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping src/main.c -o main.o
mkdir -p dist
clang -fprofile-instr-generate -fcoverage-mapping lib.o main.o  -o dist/main.bin
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping -Isrc test/test.c -o test.o
clang -fprofile-instr-generate -fcoverage-mapping lib.o test.o -lcheck -lm -lrt -lpthread -lsubunit -o dist/test.bin
./dist/main.bin "./assets/input.txt" "./dist/output.txt"
{1 Semen 100 {5 5 5} PROGA}
{1 Kirill 98 {3 3 3} FIZRA}
{0 Stepan 100 {4 5 3} PROGA}
Результат виконання программи: 
{0 Stepan 100 {4 5 3} PROGA}
{1 Semen 100 {5 5 5} PROGA}
{3937  0 {0 0 0} }
```

Також після виконання программи ви знайдете файл, у який було відведено результат `dist/output.txt`

## Висновок

На цій лабораторній работі я навчився робити зі структурованими типами данних в мові програмування C.
