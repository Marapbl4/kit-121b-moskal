
#include "lib.h"
#include <check.h>

START_TEST(str_to_digit_test)
{

    /* Створення потрібних зміниих для роботи */
    struct KontrolWork *students_kontrol_works =
            malloc(STUDENT_THAT_ENDED_EXAM * sizeof(struct KontrolWork));

    /* Читання елементів з файлу */
    get_from_file("./assets/input.txt", students_kontrol_works);


    /* Функція для роботи з колекцією (Сорутвання по кількості теоретичних питань) */
    struct KontrolWork *sorted_students_kontrol_works = sort_works(students_kontrol_works, 3);

    /* Перевірення результатів */
    ck_assert_int_eq((sorted_students_kontrol_works + 0)->work_structure.amount_of_theoretical_questions, 3);

}
END_TEST

int main()
{
    Suite *s = suite_create("Programming");
    TCase *tc_core = tcase_create("lab14");

    tcase_add_test(tc_core, str_to_digit_test);
    suite_add_tcase(s, tc_core);

    SRunner *sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
