
#include "lib.h"

/**
 * Точка входу
 * @return код завершення (0)
 */
int main(int argc, char **argv)
{
	/* Створення змінних, що зберігають у собі дирректорії з вхідним файлом та вихідним файлом */
	char *input_dir = *(argv + 1), *output_dir = *(argv + 2);

	/* Створення потрібних зміниих для роботи */
	struct KontrolWork *students_kontrol_works =
		malloc(STUDENT_THAT_ENDED_EXAM * sizeof(struct KontrolWork));

	/* Читання елементів з файлу */
	get_from_file(input_dir, students_kontrol_works);

	/* Функція для роботи з колекцією (Сорутвання по кількості теоретичних питань) */
	struct KontrolWork *sorted_students_kontrol_works =
		sort_works(students_kontrol_works, 5);

	/* Виведення елементів масиву на єкран */
	printf("Результат виконання программи: \n");
	print_structure(sorted_students_kontrol_works);

	/* Записування данних в файл */
	put_in_file(output_dir, sorted_students_kontrol_works);

	/* Очищення памьяті та завершення роботи программи */
	free(students_kontrol_works);
	free(sorted_students_kontrol_works);
	return 0;
}
