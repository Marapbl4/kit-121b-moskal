/** Структура, що слугує контейнером для динамічного масиву зі списком студентів */
struct StudentsArrayContainer {
	struct KontrolWork *array;
	int size;
};

/** Функція, що стврює масив з вказаною кільеістью елементів
 * @param container Контейнер динамічного масиву, який буде заповнено 
 * @param size Кількість елементів, які буде додано  в масиві */
struct StudentsArrayContainer *init_dynamic_array(int size);

/** Функція, що додає елемент в динамічний массив
 * @param container Контейнер динамічного масиву
 * @param position Місце у масиві, в яке буде записан елемент
 * @param element Елемент, що будемо записувати у масив */
void insert(struct StudentsArrayContainer *container, int position,
	    struct KontrolWork *element);

/** Функція, що видаля елемент з масиву
 * @param container Контейнер динамічного масиву з якого буде видалено елемент
 * @param position Місце елемента, який буде видалений */
void delete_item(struct StudentsArrayContainer *container, int position);