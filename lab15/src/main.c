

#include "lib.h"

/**
 * Точка входу
 * @return код завершення (0)
 */
int main(int argc, char **argv)
{
	/* Створення змінних, що зберігають у собі дирректорії з вхідним файлом та вихідним файлом */
	char *input_dir = *(argv + 1), *output_dir = *(argv + 2);

	/* Створення потрібних зміниих для роботи */
	int students_array_size = 3; // editable
	struct StudentsArrayContainer *students_array_container =
		init_dynamic_array(students_array_size);

	/* Читання елементів з файлу */
	get_from_file(input_dir, students_array_container);

	/* Функція для роботи з колекцією (Сорутвання по кількості теоретичних питань) */
	sort_works(students_array_container, 5);

	/* Виведення елементів масиву на єкран */
	printf("Результат: \n");
	print_structure(students_array_container);

	/* Записування данних в файл */
	put_in_file(output_dir, students_array_container);

	/* Очищення памьяті та завершення роботи программи */
	free(students_array_container);
	return 0;
}
