

#include "lib.h"
#include <check.h>

/* Константа, що потрібна лише для перевірки правильності роботи коду (кількість студентів, що здали екзамен) */
#define STUDENT_THAT_ENDED_EXAM 3

START_TEST(str_to_digit_test)
{
    /* Створення потрібних зміниих для роботи */
    struct StudentsArrayContainer * students_kontrol_works = init_dynamic_array(STUDENT_THAT_ENDED_EXAM);

    /* Читання елементів з файлу */
    get_from_file("./assets/input.txt", students_kontrol_works);

    /* Функція для роботи з колекцією (Сорутвання по кількості теоретичних питань) */
    sort_works(students_kontrol_works, 3);

    /* Перевірення результатів */
    ck_assert_int_eq((students_kontrol_works->array + 0)->work_structure.amount_of_theoretical_questions, 3);

}
END_TEST

int main()
{
    Suite *s = suite_create("Programming");
    TCase *tc_core = tcase_create("lab14");

    tcase_add_test(tc_core, str_to_digit_test);
    suite_add_tcase(s, tc_core);

    SRunner *sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
